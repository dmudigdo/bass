---
layout: default
title: About Didi Mudigdo
---

#About

##The Short Version

Hi there, I am a bass guitarist based in Perth Australia with over 25 years of performance experience. My bass influences include Ray Brown, Jaco Pastorius, Louis Johnson, Sting, Pino Palladino, John Taylor (Duran Duran), Mikey Craig (Culture Club), Martin Kemp (Spandau Ballet), and Deon Estus (Wham!).

##The Long Version

###Early Years

My parents parents arrived in Australia from Indonesia in the early 1960's as part of the Colombo Plan scholarship programme. Born in Melbourne to a musical family, my father taught me three-chord ukulele songs when I was seven. I had a wandering childhood, living in Australia, New Zealand, Indonesia, Sri Lanka and Malaysia before the age of 12. After starting my first 'jazz band' playing piano in Jakarta in 1985, I eventually made my way to back to Australia in 1986, where I had my first bass guitar, a Yamaha RBX 350.

###Sydney Start

I cut my teeth on bass in various university bands during my undergraduate years while studying  Electrical Engineering. I performed at Med & Law Revues, university musicals, band competitions and the jazz big band. After finishing my 'real' degree, I was accepted into the Sydney Conservatorium and graduated in 1997, having studied under Sydney's leading bass educators Craig Scott and Ron Philpot as well as faculty members Dick Montz, Judy Bailey, Paul McNamara and Mike Nock. The bass highlights of my 17 years in Sydney were playing in TJ Eckleberg's band alongside Reza Achman, Wendy Anggerani's fusion band Tembang and touring Asia/New Zealand with disco covers band Gotham City Horns alongside a rotation of drummers Andrew Massey, Glenn Wilson and Dave Hatch.

###Adventures in Asia on Piano

In 2005, I left Sydney for Shanghai as part of Nikki Doll and The Voodoo Blue for a 6-month engagement in Shanghai and Beijing, but playing piano/keyboards. My basses were left behind in Sydney, and for 4 years abroad in China, Japan and Southeast Asia, I played piano exclusively.

###Back on the Bass: Perth via Sydney and Asia

Returning to Sydney in 2009, I met the wonderful Singaporean singer-songwriter Juliet Pang at a jazz open mic. We soon formed the popular ukulele duo Polkadot + Moonbeam and piano duo Mint Street, and both of these duos had me back on the bass. We were based from 2011 in Singapore, from which we toured Australia, Japan, China, Singapore and Malaysia. Juliet and I married in Singapore in 2013, and in 2016 settled in Perth Australia as music educators, sharing our extensive music performance experience.